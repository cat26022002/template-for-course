# Template for backend course
### Запуск БД
```commandline
make db
```
### Запуск сервера
```commandline
make app
```
При использование эндпоинта me/ передавайте id в заголовке запроса
### Запуск бота
```commandline
make bot
```