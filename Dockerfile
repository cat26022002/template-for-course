FROM python:3.10-slim

ENV PYTHONUNBUFFERED 1
ENV PYTHONDONTWRITEBYTECODE 1

WORKDIR /app

COPY Pipfile .
COPY Pipfile.lock .


RUN apt-get update && \
    apt-get install -y --no-install-recommends gcc && \
    pip install pipenv

RUN pipenv install --system --deploy --ignore-pipfile


COPY src .
CMD ["./entrypoint.sh"]