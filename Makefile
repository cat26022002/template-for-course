include .env

migrate:
	docker-compose run --rm app python3 manage.py migrate

makemigrations:
	docker-compose run --rm app python3 manage.py makemigrations

createsuperuser:
	docker-compose run --rm app python3 manage.py createsuperuser

collectstatic:
	docker-compose run --rm app python3 manage.py collectstatic --no-input

shell:
	docker-compose run --rm app python3 manage.py shell

debug:
	docker-compose run --rm app python3 manage.py debug

piplock:
	pipenv install
	sudo chown -R ${USER} Pipfile.lock

lint:
	isort .
	flake8 --config setup.cfg
	black --config pyproject.toml .

check_lint:
	isort --check --diff .
	flake8 --config setup.cfg
	black --check --config pyproject.toml .

bot:
	docker-compose up -d bot --build

app:
	docker-compose up -d app --build

db:
	docker-compose up -d db --build

build:
	docker-compose build

pull:
	docker pull ${IMAGE_APP}

push:
	docker push ${IMAGE_APP}

docker_lint:
	docker-compose run app isort --check --diff .
	docker-compose run app flake8 --config setup.cfg
	docker-compose run app black --check --config pyproject.toml .

test:
	docker-compose run app pytest tests

down:
	docker-compose down

up:
	docker-compose up -d

