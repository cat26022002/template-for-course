from http import HTTPStatus
import pytest
from django.test import Client


@pytest.mark.smoke
@pytest.mark.django_db
def test_api():
    client = Client(headers={"Id": "461479962"})
    response = client.get("/me/")
    assert response.status_code == HTTPStatus.NOT_FOUND
