import factory

from app.internal.models.user import User
from app.internal.models.bank_account import BankAccount


class FakeUser(factory.django.DjangoModelFactory):
    class Meta:
        model = User

    id = factory.Faker("pyint")
    name = factory.Faker("first_name")
    phone_number = factory.Faker("phone_number")


class FakeBankAccount(factory.django.DjangoModelFactory):
    class Meta:
        model = BankAccount

    id = factory.Faker("pyint")
    number = factory.Faker("iban")
    owner = factory.SubFactory(FakeUser)
    money = factory.Faker("pydecimal", left_digits=3, right_digits=2, positive=True)
