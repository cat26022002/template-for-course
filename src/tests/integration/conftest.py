from unittest.mock import MagicMock, PropertyMock

import pytest

from app.internal.models.bank_account import BankAccount
from app.internal.models.user import User

from tests.fakes import FakeUser, FakeBankAccount


@pytest.fixture
def update():
    user = MagicMock()
    type(user).id = PropertyMock(return_value=123)
    type(user).username = PropertyMock(return_value="username")
    type(user).first_name = PropertyMock(return_value="first_name")
    type(user).last_name = PropertyMock(return_value="last_name")

    message = MagicMock()
    message.reply_text.return_value = None
    message.text = ""

    update = MagicMock()
    update.effective_user = user
    update.message = message

    return update


@pytest.fixture
def context():
    context = MagicMock()
    context.args = []
    return context


@pytest.fixture
def user_first():
    user_fake = FakeUser()
    user_fake.id = 3
    cur_user = User.objects.create(id=user_fake.id, name=user_fake.name, phone_number=user_fake.phone_number)
    return cur_user


@pytest.fixture
def user_second():
    user_fake = FakeUser()
    user_fake.id = 4
    cur_user = User.objects.create(id=user_fake.id, name=user_fake.name, phone_number=user_fake.phone_number)
    return cur_user


@pytest.fixture
def bank_account_first():
    bank_account_fake = FakeBankAccount(id=1, money=100)
    bank_account_fake.owner.id = 5
    bank_account_fake.id = 5
    cur_user = User.objects.create(
        id=bank_account_fake.owner.id,
        name=bank_account_fake.owner.name,
        phone_number=bank_account_fake.owner.phone_number)
    cur_bank_account = BankAccount.objects.create(
        id=bank_account_fake.id,
        number=bank_account_fake.number,
        money=bank_account_fake.money,
        owner=cur_user
    )
    return cur_bank_account


@pytest.fixture
def bank_account_second():
    bank_account_fake = FakeBankAccount(id=2, money=100)
    bank_account_fake.owner.id = 6
    bank_account_fake.id = 6
    cur_user = User.objects.create(
        id=bank_account_fake.owner.id,
        name=bank_account_fake.owner.name,
        phone_number=bank_account_fake.owner.phone_number)
    cur_bank_account = BankAccount.objects.create(
        id=bank_account_fake.id,
        number=bank_account_fake.number,
        money=bank_account_fake.money,
        owner=cur_user
    )
    return cur_bank_account
