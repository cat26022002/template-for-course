import pytest
from app.internal.transport.bot.handlers import start, save_phone, add_user_to_favorite_list, \
    send_money_by_account_number
from telegram import Update
from telegram.ext import CallbackContext
from app.internal.models.bank_account import BankAccount
from app.internal.models.user import User


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.asyncio
async def test_start(update: Update, context: CallbackContext):
    await start(update, context)
    user = await User.objects.aget(id=update.effective_user.id)
    assert user.username == update.effective_user.name


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.asyncio
async def test_set_phone(update: Update, context: CallbackContext):
    await start(update, context)
    update.effective_message.contact.phone_number = "89991767899"
    await save_phone(update, context)
    user = await User.objects.aget(id=update.effective_user.id)
    assert user.phone_number == update.effective_message.contact.phone_number


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.asyncio
async def test_add_user_to_favorite_list(update: Update, context: CallbackContext, user_first: User, user_second: User):
    update.effective_user.id = user_first.id
    context.args = [user_second.name]
    await add_user_to_favorite_list(update, context)
    user = await User.objects.aget(id=update.effective_user.id)
    assert user_second.name in user.favorite_users


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.asyncio
async def test_send_money_by_account_number(update: Update,
                                            context: CallbackContext,
                                            user_first: User,
                                            user_second: User,
                                            bank_account_first: BankAccount,
                                            bank_account_second: BankAccount):
    update.effective_user.id = user_first.id
    context.args = [user_second.name]
    await add_user_to_favorite_list(update, context)
    context.args = [bank_account_second, 10]
    await send_money_by_account_number(update, context)
    b1 = await BankAccount.objects.aget(id=bank_account_first.id)
    b2 = await BankAccount.objects.aget(id=bank_account_second.id)
    assert b1.money + 10 == bank_account_first.money
