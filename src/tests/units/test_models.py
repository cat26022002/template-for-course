import pytest
from django.core.exceptions import ValidationError
from app.internal.models.bank_account import BankAccount
from app.internal.models.card import Card
from app.internal.models.user import User
from app.internal.services.user_service import update_or_create_user
from app.internal.transport.bot.handlers import get_normalized_telegram_name


@pytest.mark.unit
@pytest.mark.django_db
def test_card_number_validator_raise():
    card = Card(id=1, number=12)
    with pytest.raises(ValidationError):
        card.full_clean()


@pytest.mark.unit
@pytest.mark.django_db
def test_card_expiry_validator_raise():
    card = Card(id=1, expiry='01/2001')
    with pytest.raises(ValidationError):
        card.full_clean()


@pytest.mark.unit
@pytest.mark.django_db
def test_bank_account_validator_raise():
    account = BankAccount(id=1, number=12, money=0)
    with pytest.raises(ValidationError):
        account.full_clean()


@pytest.mark.unit
@pytest.mark.django_db
def test_phone_validator():
    user = User(id=1, name="kek", phone_number="7-700-556-38-35")
    user.full_clean()
    assert user.phone_number == "7-700-556-38-35"


@pytest.mark.unit
@pytest.mark.parametrize(("name", "result"), [("kek", "kek"), ("@kek", "kek")])
def test_get_normalized_username(name, result):
    assert get_normalized_telegram_name(name) == result


@pytest.mark.unit
@pytest.mark.django_db
@pytest.mark.asyncio
async def test_create_personal_data():
    await update_or_create_user(5, "kek")

    user = await User.objects.aget(id=5)
    assert user.username == "kek"
