from django.contrib import admin

from app.internal.authentication.presentation.admin import TokenAdmin
from app.internal.bank_account.presentation.admin import BankAccountAdmin
from app.internal.card.presentation.admin import CardAdmin
from app.internal.user.presentation.admin import AdminUser
from app.internal.user.presentation.admin import UserAdmin


admin.site.site_title = "Backend course"
admin.site.site_header = "Backend course"
