from ninja import Schema


class UserSchema(Schema):
    id: int
    name: str


class UserOut(UserSchema):
    phone_number: str | None = None


class UserIn(UserSchema):
    pass


class UsersResponse(Schema):
    users: list[str]
