from app.internal.user.db.models import User
from app.internal.user.db.repositories import UserRepository, IUserRepository
from app.internal.user.domain.entities import UserIn, UserOut
from functools import lru_cache


class WrongPasswordError(Exception):
    pass


class NoUserError(Exception):
    pass


class UserService:
    def __init__(self, user_repository: IUserRepository):
        self._user_repository = user_repository

    def update_or_create_user(self, user_model: UserIn) -> UserOut:
        return self._user_repository.update_or_create_user(user_model)

    def get_user(self, telegram_id: int) -> User | None:
        return self._user_repository.try_get_user(telegram_id)

    def get_user_by_username(self, username: str) -> User | None:
        return self._user_repository.try_get_user_by_name(username)

    def check_password(self, user: User, password: str) -> bool:
        return self._user_repository.check_password(user, password)

    def try_save_phone(self, telegram_id: int, phone: str) -> bool:
        return self._user_repository.try_save_phone(telegram_id, phone)

    def set_password_for_user(self, telegram_id: int, password: str) -> None:
        if self._user_repository.set_password_for_user(telegram_id, password):
            return None
        raise WrongPasswordError

    def get_user_favorites(self, telegram_id: int) -> list[str]:
        return self._user_repository.get_user_favorites(telegram_id)

    def add_favorite_user(self, telegram_id: int, favorite_user_name: str):
        if self._user_repository.add_favorite_user(telegram_id, favorite_user_name):
            return None
        raise NoUserError

    def delete_favorite_user(self, telegram_id: int, favorite_user_name: str) -> bool:
        return self._user_repository.delete_favorite_user(telegram_id, favorite_user_name)


@lru_cache(maxsize=1)
def get_user_service() -> UserService:
    return UserService(user_repository=UserRepository())
