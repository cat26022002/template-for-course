from django.core.exceptions import ObjectDoesNotExist
from app.internal.user.db.models import User
from app.internal.user.domain.entities import UserIn, UserOut
from config import settings
import hashlib
import abc
from typing import Optional


def get_hash_password(password: str):
    return hashlib.sha512(password.encode() + settings.SALT.encode()).hexdigest()


class IUserRepository(abc.ABC):
    @abc.abstractmethod
    def update_or_create_user(self, user_model: UserIn) -> UserOut:
        pass

    @abc.abstractmethod
    def try_get_user(self, telegram_id: int) -> Optional[User]:
        pass

    @abc.abstractmethod
    def try_get_user_by_name(self, username: str) -> Optional[User]:
        pass

    @abc.abstractmethod
    def check_password(self, user: User, password: str) -> bool:
        pass

    @abc.abstractmethod
    def try_save_phone(self, telegram_id: int, phone: str) -> bool:
        pass

    @abc.abstractmethod
    def set_password_for_user(self, telegram_id: int, password: str) -> bool:
        pass

    @abc.abstractmethod
    def get_user_favorites(self, telegram_id: int) -> list[str]:
        pass

    @abc.abstractmethod
    def add_favorite_user(self, telegram_id: int, favorite_user_name: str) -> bool:
        pass

    @abc.abstractmethod
    def delete_favorite_user(self, telegram_id: int, favorite_user_name: str) -> None:
        pass


class UserRepository(IUserRepository):
    def update_or_create_user(self, user_model: UserIn) -> UserOut:
        user, created = User.objects.update_or_create(id=id, defaults={"name": UserIn.name})
        return UserOut.from_orm(user)

    def try_get_user(self, telegram_id: int) -> Optional[User]:
        try:
            return User.objects.get(id=id)
        except ObjectDoesNotExist:
            return None

    def try_get_user_by_name(self, username: str) -> Optional[User]:
        try:
            return User.objects.get(name=username)
        except ObjectDoesNotExist:
            return None

    def check_password(self, user: User, password: str) -> bool:
        return user.password == get_hash_password(password)

    def try_save_phone(self, telegram_id: int, phone: str) -> bool:
        try:
            User.objects.filter(id=id).update(phone_number=phone)
            return True
        except ObjectDoesNotExist:
            return False

    def set_password_for_user(self, user_id: int, password: str) -> bool:
        user = self.try_get_user(user_id)
        if user is None:
            return False
        user.password = get_hash_password(password)
        user.save(update_fields=["password"])
        return True

    def get_user_favorites(self, user_id: int) -> list[str]:
        user = self.try_get_user(user_id)
        if user is None:
            return []
        user_names_from_favorites = user.favorite_users.values_list("name", flat=True)
        return list(user_names_from_favorites)

    def add_favorite_user(self, user_id: int, favorite_user_name: str) -> bool:
        user = self.try_get_user(user_id)
        if user is None:
            return False
        favorite_user = self.try_get_user_by_name(favorite_user_name)
        if favorite_user is None:
            return False
        user_exits_in_list = user.favorite_users.filter(name__iexact=favorite_user.name).aexists()
        if user_exits_in_list:
            return False
        user.favorite_users.add(favorite_user)
        user.save()
        return True

    def delete_favorite_user(self, user_id: int, favorite_user_name: str) -> bool:
        user = self.try_get_user(user_id)
        if user is None:
            return False
        favorite_user = self.try_get_user_by_name(favorite_user_name)
        if favorite_user is None:
            return False
        user_exits_in_list = user.favorite_users.filter(name__iexact=favorite_user.name).exists()
        if not user_exits_in_list:
            return False
        user.favorite_users.remove(favorite_user)
        user.save()
        return True
