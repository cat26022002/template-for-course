from django.db import models
from django.contrib.auth.models import AbstractUser


class User(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=255)
    phone_number = models.CharField(max_length=50, default=None, null=True)
    favorite_users = models.ManyToManyField("self", symmetrical=False)
    password = models.CharField(max_length=128, default=None, null=True)

    class Meta:
        verbose_name = "User data"

    def __str__(self):
        return f"id: {self.id} name: {self.name} phone number: {self.phone_number}"

class AdminUser(AbstractUser):
    pass
