from app.internal.transport.bot.messages import start_response, me_response, save_phone_asking, save_phone_response, \
    name_not_exist, remove_user_from_favorite
from app.internal.user.domain.entities import UserIn
from app.internal.user.domain.services import get_user_service
from telegram import KeyboardButton, ReplyKeyboardMarkup, Update
from telegram.ext import CallbackContext


def start(update: Update, context: CallbackContext) -> None:
    result = get_user_service().update_or_create_user(UserIn(
        name=update.effective_user.username,
        id=update.effective_user.id
    ))
    update.message.reply_text(text=start_response())


def me(update: Update, context: CallbackContext) -> None:
    user = get_user_service().get_user(update.effective_user.id)
    update.message.reply_text(text=me_response(user.id, user.name, user.phone_number))


def save_phone_request(update: Update, context: CallbackContext) -> None:
    reply_markup = ReplyKeyboardMarkup(
        [[KeyboardButton(text="Отправить телефон", request_contact=True)]], one_time_keyboard=True
    )
    update.message.reply_text(text=save_phone_asking(), reply_markup=reply_markup)


def save_phone(update: Update, context: CallbackContext) -> None:
    get_user_service().try_save_phone(update.effective_user.id, update.effective_message.contact.phone_number)
    update.message.reply_text(text=save_phone_response(), reply_markup=ReplyKeyboardMarkup([]))


def get_favourites(update: Update, context: CallbackContext) -> None:
    favourites = get_user_service().get_user_favorites(update.effective_user.id)
    update.message.reply_text(text=favourites)


def add_to_favourites(update: Update, context: CallbackContext) -> None:
    if len(context.args) < 1:
        update.message.reply_text(text=name_not_exist())
        return
    favourites = get_user_service().add_favorite_user(update.effective_user.id, context.args[0])
    update.message.reply_text(text=favourites)


def remove_from_favourites(update: Update, context: CallbackContext) -> None:
    if len(context.args) < 1:
        update.message.reply_text(text=name_not_exist())
        return
    get_user_service().delete_favorite_user(update.effective_user.id, context.args[0])
    update.message.reply_text(text=remove_user_from_favorite())
