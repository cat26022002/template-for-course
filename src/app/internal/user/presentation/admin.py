from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from app.internal.user.db.models import AdminUser
from app.internal.user.db.models import User


@admin.register(AdminUser)
class AdminUserAdmin(UserAdmin):
    pass


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    pass
