from http import HTTPStatus

from django.http import HttpRequest, JsonResponse
from pydantic.types import SecretStr

from app.internal.user.domain.entities import UsersResponse
from app.internal.user.domain.services import NoUserError, UserService, WrongPasswordError


class UserHandlers:
    def __init__(self, user_service: UserService):
        self._user_service = user_service

    def get_favorites(self, request: HttpRequest):
        favorites = self._user_service.get_user_favorites(request.user.telegram_id)
        if not favorites:
            return JsonResponse({"detail": "no favorites"}, status=HTTPStatus.NOT_FOUND)

        return UsersResponse(users=favorites)

    def add_favorites(self, request: HttpRequest, username: str):
        try:
            self._user_service.add_favorite_user(request.user.telegram_id, username)
        except NoUserError:
            return JsonResponse({"detail": f"no user {username}"}, status=HTTPStatus.NOT_FOUND)

        return JsonResponse(status=HTTPStatus.OK)

    def delete_favorites(self, request: HttpRequest, username: str):
        self._user_service.delete_favorite_user(request.user.telegram_id, username)

        return JsonResponse(status=HTTPStatus.OK)

    def set_password(self, request: HttpRequest, password: str):
        try:
            self._user_service.set_password_for_user(request.user.telegram_id, SecretStr(password))
        except WrongPasswordError:
            return JsonResponse({"detail": "fail"}, status=HTTPStatus.BAD_REQUEST)

        return JsonResponse(status=HTTPStatus.OK)
