from ninja import NinjaAPI, Router

from app.internal.user.presentation.handlers import UserHandlers


def get_user_router(user_handlers: UserHandlers):
    router = Router(tags=["user"])

    router.add_api_operation(
        "/favorites",
        ["GET"],
        user_handlers.get_favorites,
    )

    router.add_api_operation(
        "/add_favorite",
        ["POST"],
        user_handlers.add_favorites,
    )

    router.add_api_operation(
        "/delete_favorite",
        ["DELETE"],
        user_handlers.delete_favorites,
    )

    router.add_api_operation(
        "/change_password",
        ["PUT"],
        user_handlers.set_password,
    )

    return router


def add_user_router(api: NinjaAPI, user_handlers: UserHandlers):
    user_router = get_user_router(user_handlers)
    api.add_router("/user", user_router)
