from ninja import NinjaAPI, Router

from app.internal.bank_account.presentation.handlers import AccountHandlers


def get_account_router(account_handlers: AccountHandlers):
    router = Router(tags=["account"])

    router.add_api_operation(
        "/sent_to",
        ["POST"],
        account_handlers.send_to,
    )

    return router


def add_account_router(api: NinjaAPI, account_handlers: AccountHandlers):
    user_router = get_account_router(account_handlers)
    api.add_router("/account", user_router)
