from http import HTTPStatus

from django.http import HttpRequest, JsonResponse

from app.internal.bank_account.domain.services import (
    AccountService,
    NotAccountInfoError,
    NotEnoughMoneyError,
    SendYourselfError,
)


class AccountHandlers:
    def __init__(self, account_service: AccountService):
        self._account_service = account_service

    def send_to(self, request: HttpRequest, username: str, count: int):
        try:
            self._account_service.send_to(request.user.telegram_id, username, count)
        except NotAccountInfoError:
            return JsonResponse({"detail": "no account"}, status=HTTPStatus.NOT_FOUND)
        except SendYourselfError:
            return JsonResponse({"detail": "forbidden to send yourself"}, status=HTTPStatus.METHOD_NOT_ALLOWED)
        except NotEnoughMoneyError:
            return JsonResponse({"detail": "no enough money"}, status=HTTPStatus.BAD_REQUEST)

        return JsonResponse(status=HTTPStatus.OK)
