from django.contrib import admin

from app.internal.bank_account.db.models import BankAccount


@admin.register(BankAccount)
class BankAccountAdmin(admin.ModelAdmin):
    pass
