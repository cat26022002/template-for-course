from telegram import Update
from telegram.ext import CallbackContext

from app.internal.bank_account.domain.services import get_bank_service
from app.internal.transport.bot.messages import name_not_exist, send_money_by_name_argument_error, send_money_succes
from app.internal.transport.bot.messages import bank_account_data


def get_bank_account(update: Update, context: CallbackContext) -> None:
    if len(context.args) < 1:
        update.message.reply_text(text=name_not_exist())
        return
    account_info = get_bank_service().get_account(context.args[1])
    update.message.reply_text(text=bank_account_data(account_info))


def send_to_user(update: Update, context: CallbackContext) -> None:
    if len(context.args) < 2 or not context.args[1].isdigit():
        update.message.reply_text(text=send_money_by_name_argument_error())
        return
    get_bank_service().send_to(update.effective_user.id, context.args[0], context.args[1])
    update.message.reply_text(text=send_money_succes())


def send_to_account(update: Update, context: CallbackContext) -> None:
    if len(context.args) < 2 or not context.args[1].isdigit() or not context.args[0].isdigit():
        update.message.reply_text(text=send_money_by_name_argument_error())
        return
    get_bank_service().send_to_account(update.effective_user.id, context.args[0], context.args[1])
    update.message.reply_text(text=send_money_succes())


def send_to_card(update: Update, context: CallbackContext) -> None:
    if len(context.args) < 2 or not context.args[0].isdigit() or not context.args[1].isdigit():
        update.message.reply_text(text=send_money_by_name_argument_error())
        return
    get_bank_service().send_to_card(update.effective_user.id, context.args[0], context.args[1])
    update.message.reply_text(text=send_money_succes())

