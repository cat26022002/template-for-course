from ninja import Schema

from app.internal.card.domain.entities import CardInfo


class BankAccountSchema(Schema):
    number: str
    amount: float
    cards: list[CardInfo]
