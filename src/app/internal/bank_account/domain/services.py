from app.internal.bank_account.db.models import BankAccount
from app.internal.bank_account.db.repositories import IBankAccountStorage, BankAccountStorage
from app.internal.transaction.db.repositories import BankTransactionStorage
from functools import lru_cache


class NotAccountInfoError(Exception):
    pass


class SendYourselfError(Exception):
    pass


class NotEnoughMoneyError(Exception):
    pass


class AccountService:
    def __init__(self, account_repo: IBankAccountStorage, transaction_repo: BankTransactionStorage):
        self._account_repo = account_repo
        self._transaction_repo = transaction_repo

    def try_send_money(self, account_from: BankAccount, account_to: BankAccount, count: int) -> bool:
        return self._account_repo.try_send_money(account_from, account_to, count)

    def get_account(self, number: str) -> BankAccount | None:
        return self._account_repo.get_account(number)

    def get_account_by_card(self, number: str) -> BankAccount | None:
        return self._account_repo.get_account_by_card(number)

    def get_account_by_telegram_id(self, telegram_id: int) -> BankAccount:
        return self.get_account_by_telegram_id(telegram_id)

    def get_account_by_username(self, username: str) -> BankAccount:
        return self._account_repo.get_account_by_username(username)

    def user_has_account(self, telegram_id: int, account_number: str) -> bool:
        return self._account_repo.user_has_account(telegram_id, account_number)

    def send_to(self, telegram_id: int, receiver_name: str, count: int) -> None:
        account_to = self._account_repo.get_account_by_username(receiver_name)
        if account_to is None:
            raise NotAccountInfoError

        return self._transfer_money(telegram_id, account_to, count)

    def send_to_account(self, telegram_id: int, receiver_account: str, count: int) -> None:
        account_to = self._account_repo.get_account(receiver_account)
        if account_to is None:
            raise NotAccountInfoError

        self._transfer_money(telegram_id, account_to, count)

    def send_to_card(self, telegram_id: int, receiver_card: str, count: int) -> None:
        account_to = self._account_repo.get_account_by_card(receiver_card)
        if account_to is None:
            raise NotAccountInfoError

        self._transfer_money(telegram_id, account_to, count, receiver_card)

    def _transfer_money(
            self, telegram_id: int, account_to: BankAccount, count: int, receiver_card: str | None = None
    ) -> None:
        if account_to.owner.id == telegram_id:
            raise SendYourselfError

        account_from = self._account_repo.get_account_by_telegram_id(telegram_id)
        if account_from is None:
            raise NotAccountInfoError

        if not self._account_repo.try_send_money(account_from, account_to, count):
            raise NotEnoughMoneyError

        self._transaction_repo.save_transaction(account_from, account_to, count, receiver_card)


@lru_cache(maxsize=1)
def get_bank_service() -> AccountService:
    return AccountService(BankAccountStorage(), BankTransactionStorage())
