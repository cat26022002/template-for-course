from app.internal.card.db.models import Card
from app.internal.bank_account.db.models import BankAccount

import abc

from django.db import transaction
from django.db.models import F, Q


class IBankAccountStorage(abc.ABC):
    @abc.abstractmethod
    def try_send_money(self, account_from: BankAccount, account_to: BankAccount, count: int) -> bool:
        pass

    @abc.abstractmethod
    def get_account(self, number: str) -> BankAccount | None:
        pass

    @abc.abstractmethod
    def get_account_by_card(self, number: str) -> BankAccount | None:
        pass

    @abc.abstractmethod
    def get_account_by_telegram_id(self, telegram_id: int) -> BankAccount:
        pass

    @abc.abstractmethod
    def get_account_by_username(self, username: str) -> BankAccount:
        pass

    @abc.abstractmethod
    def user_has_account(self, telegram_id: int, account_number: str) -> bool:
        pass


class BankAccountStorage(IBankAccountStorage):
    @transaction.atomic()
    def try_send_money(self, account_from: BankAccount, account_to: BankAccount, amount: int) -> bool:
        if amount < 0:
            return False

        if account_from.amount < amount:
            return False

        account_from.money = account_from.money - amount
        account_from.save(update_fields=["money"])
        account_to.money = account_to.money + amount
        account_to.save(update_fields=["money"])
        return True

    def get_account(self, number: str) -> BankAccount | None:
        return BankAccount.objects.filter(number=number).first()

    @transaction.atomic()
    def get_account_by_card(self, number: str) -> BankAccount | None:
        account = Card.objects.filter(number=number)
        if not account.exists():
            return None
        return account.first().account

    def get_account_by_telegram_id(self, telegram_id: int) -> BankAccount:
        return BankAccount.objects.filter(owner_id=telegram_id).first()

    def get_account_by_username(self, username: str) -> BankAccount:
        return BankAccount.objects.filter(owner__name=username).first()

    def user_has_account(self, telegram_id: int, account_number: str) -> bool:
        card = BankAccount.objects.filter(Q(number=account_number) & Q(owner__id=telegram_id))
        return card.exists()
