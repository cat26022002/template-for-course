from django.db import models


class BankAccount(models.Model):
    id = models.IntegerField(primary_key=True)
    number = models.CharField(max_length=255)
    owner = models.ForeignKey("User", on_delete=models.PROTECT, verbose_name="owner", related_name="bank_accounts")
    money = models.DecimalField(max_digits=10, decimal_places=2, verbose_name="money")

    class Meta:
        verbose_name = "account"
        verbose_name_plural = "accounts"
        db_table = "bank_accounts"
