from django.contrib import admin

from app.internal.card.db.models import Card


@admin.register(Card)
class CardAdmin(admin.ModelAdmin):
    pass
