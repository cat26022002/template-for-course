from app.internal.card.domain.services import IBankCardStorage
from app.internal.card.db.models import Card
from django.db.models import Q


class BankCardStorage(IBankCardStorage):
    def user_has_card(self, telegram_id: int, card_number: str) -> bool:
        card = Card.objects.filter(Q(number=card_number) & Q(account__owner__id=telegram_id))
        return card.exists()