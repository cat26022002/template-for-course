from creditcards.models import CardExpiryField, CardNumberField, SecurityCodeField
from django.db import models


class Card(models.Model):
    id = models.IntegerField(primary_key=True)
    number = CardNumberField(verbose_name="card number")
    expiry = CardExpiryField(verbose_name="expiration date")
    code = SecurityCodeField(verbose_name="security code")
    bank_account = models.ForeignKey("BankAccount", on_delete=models.PROTECT, verbose_name="account",
                                     related_name="cards")

    class Meta:
        verbose_name = "card"
        verbose_name_plural = "cards"
        db_table = "card"
