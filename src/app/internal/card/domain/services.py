import abc


class IBankCardStorage(abc.ABC):
    @abc.abstractmethod
    def user_has_card(self, telegram_id: int, card_number: str) -> bool:
        pass


class CardService:
    def __init__(self, card_repo: IBankCardStorage):
        self._card_repo = card_repo

    def user_has_card(self, telegram_id: int, card_number: str) -> bool:
        return self._card_repo.user_has_card(telegram_id, card_number)
