from ninja import NinjaAPI

from app.internal.authentication.db.repositories import AuthRepository
from app.internal.authentication.domain.services import AuthService
from app.internal.authentication.presentation.handlers import AuthHandlers, HTTJWTAuth
from app.internal.authentication.presentation.routers import add_auth_router
from app.internal.bank_account.db.repositories import BankAccountStorage
from app.internal.bank_account.domain.services import AccountService
from app.internal.bank_account.presentation.handlers import AccountHandlers
from app.internal.bank_account.presentation.routers import add_account_router
from app.internal.card.db.repositories import BankCardStorage
from app.internal.transaction.db.repositories import BankTransactionStorage
from app.internal.transaction.domain.services import TransactionService
from app.internal.transaction.presentation.handlers import TransactionHandlers
from app.internal.transaction.presentation.routers import add_transaction_router
from app.internal.user.db.repositories import UserRepository
from app.internal.user.domain.services import UserService
from app.internal.user.presentation.handlers import UserHandlers
from app.internal.user.presentation.routers import add_user_router


def get_api():
    user_repo = UserRepository()
    auth_repo = AuthRepository()
    account_repo = BankAccountStorage()
    transaction_repo = BankTransactionStorage()
    card_repo = BankCardStorage()

    user_service = UserService(user_repository=user_repo)
    auth_service = AuthService(auth_repository=auth_repo)
    account_service = AccountService(account_repo, transaction_repo)
    transaction_service = TransactionService(transaction_repo, account_repo, card_repo)

    user_handler = UserHandlers(user_service=user_service)
    auth_handler = AuthHandlers(auth_service=auth_service, user_service=user_service)
    account_handler = AccountHandlers(account_service)
    transaction_handler = TransactionHandlers(transaction_service)

    api = NinjaAPI(
        version="1.0.0",
        auth=[HTTJWTAuth(auth_service=auth_service, user_service=user_service)],
    )

    add_auth_router(api, auth_handler)
    add_user_router(api, user_handler)
    add_account_router(api, account_handler)
    add_transaction_router(api, transaction_handler)

    return api
