from http import HTTPStatus

from django.http import HttpRequest, JsonResponse

from app.internal.transaction.domain.services import TransactionService
from app.internal.user.domain.entities import UsersResponse


class TransactionHandlers:
    def __init__(self, transaction_service: TransactionService):
        self._transaction_service = transaction_service

    def interacted_users(self, request: HttpRequest):
        users = self._transaction_service.get_interacted_users(request.user.telegram_id)
        if not users:
            return JsonResponse({"detail": "no users"}, status=HTTPStatus.NOT_FOUND)
        return UsersResponse(users=users)
