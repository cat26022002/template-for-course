from telegram import Update
from telegram.ext import CallbackContext
from app.internal.transaction.domain.services import get_transaction_service
from app.internal.transport.bot.messages import arg_error


async def interacted_users(update: Update, CallbackContext) -> None:
    name_list = get_transaction_service.get_interacted_users(update.effective_user.id)
    update.message.reply_text(text=name_list)


async def account_statement(update: Update, context: CallbackContext) -> None:
    if len(context.args) < 1 or not context.args[0].isdigit():
        update.message.reply_text(text=arg_error())
        return
    tr = get_transaction_service.get_account_transaction(update.effective_user.id, context.args[0])
    update.message.reply_text(text=tr)


async def card_statement(update: Update, context: CallbackContext) -> None:
    if len(context.args) < 1 or not context.args[0].isdigit():
        update.message.reply_text(text=arg_error())
        return
    tr = get_transaction_service.get_card_transaction(update.effective_user.id, context.args[0])
    update.message.reply_text(text=tr)


handlers = {
    "account_statement": account_statement,
    "card_statement": card_statement,
    "interacted_users": interacted_users,
}
