from ninja import NinjaAPI, Router

from app.internal.transaction.presentation.handlers import TransactionHandlers


def get_transaction_router(transaction_handlers: TransactionHandlers):
    router = Router(tags=["transaction"])

    router.add_api_operation(
        "/interacted_users",
        ["GET"],
        transaction_handlers.interacted_users,
    )

    return router


def add_transaction_router(api: NinjaAPI, transaction_handlers: TransactionHandlers):
    transaction_router = get_transaction_router(transaction_handlers)
    api.add_router("/transaction", transaction_router)
