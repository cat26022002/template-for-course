from django.contrib import admin

from app.internal.transaction.db.models import Transaction


@admin.register(Transaction)
class TransactionAdmin(admin.ModelAdmin):
    pass
