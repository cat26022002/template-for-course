from app.internal.bank_account.db.models import BankAccount
from app.internal.bank_account.db.repositories import BankAccountStorage
from app.internal.bank_account.domain.services import IBankAccountStorage
from app.internal.card.domain.services import IBankCardStorage
from app.internal.transaction.db.repositories import BankTransactionStorage, IBankTransactionStorage
from app.internal.card.db.repositories import BankCardStorage
from app.internal.transaction.domain.entities import TransactionInfo
from functools import lru_cache


class NoCardError(Exception):
    pass


class NoAccountError(Exception):
    pass


class TransactionService:
    def __init__(
            self, transaction_repo: IBankTransactionStorage, account_repo: IBankAccountStorage,
            card_repo: IBankCardStorage
    ):
        self._account_repo = account_repo
        self._transaction_repo = transaction_repo
        self._card_repo = card_repo

    def save_transaction(
            self, account_from: BankAccount, account_to: BankAccount, count: int, receiver_card: str | None
    ) -> None:
        self._transaction_repo.save_transaction(account_from, account_to, count, receiver_card)

    def get_card_transaction(self, telegram_id: int, card_number: str) -> list[TransactionInfo]:
        if not self._card_repo.user_has_card(telegram_id, card_number):
            raise NoCardError
        return self._transaction_repo.get_card_transaction(card_number)

    def get_account_transaction(self, telegram_id: int, account_number: str) -> list[TransactionInfo]:
        if not self._account_repo.user_has_account(telegram_id, account_number):
            raise NoAccountError
        return self._transaction_repo.get_account_transaction(account_number)

    def get_interacted_users(self, telegram_id: int) -> set[str]:
        return self._transaction_repo.get_interacted_users(telegram_id)


@lru_cache(maxsize=1)
def get_transaction_service() -> TransactionService:
    return TransactionService(BankTransactionStorage(), BankAccountStorage(), BankCardStorage())
