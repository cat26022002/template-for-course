from pydantic.main import BaseModel
import datetime


class TransactionInfo(BaseModel):
    user_from: str
    user_to: str
    money: float
    date: datetime.datetime

