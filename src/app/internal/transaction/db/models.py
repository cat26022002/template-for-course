from django.db import models
from django.core.validators import MinValueValidator
import uuid

from app.internal.bank_account.db.models import BankAccount
from app.internal.card.db.models import Card


class Transaction(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    account_from = models.ForeignKey(to=BankAccount, null=True, on_delete=models.CASCADE,related_name="transactions_from")
    account_to = models.ForeignKey(to=BankAccount, null=True, on_delete=models.CASCADE, related_name="transactions_to")
    card_to = models.ForeignKey(to=Card, null=True, on_delete=models.CASCADE, related_name="transactions_to")
    money = models.DecimalField(max_digits=10, decimal_places=2, validators=[MinValueValidator(limit_value=0)])
    created_at = models.DateTimeField(auto_now_add=True)
    objects = models.Manager()

    class Meta:
        verbose_name = "transaction"
        verbose_name_plural = "transactions"
        db_table = "transaction"
        constraints = [
            models.CheckConstraint(
                name="card_to_or_account_to",
                check=(
                        models.Q(card_to__isnull=True, account_to__isnull=False)
                        | models.Q(card_to__isnull=False, account_to__isnull=True)
                ),
            )
        ]
