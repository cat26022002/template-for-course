from app.internal.card.db.models import Card
from app.internal.transaction.db.models import Transaction
from django.db.models import Q
from app.internal.bank_account.db.models import BankAccount
import abc
from app.internal.transaction.domain.entities import TransactionInfo


class IBankTransactionStorage(abc.ABC):
    @abc.abstractmethod
    def save_transaction(
            self, account_from: BankAccount,
            money: int,
            account_to: BankAccount = None,
            card_to: Card = None) -> None:
        pass

    @abc.abstractmethod
    def get_card_transaction(self, card_number: str) -> list[TransactionInfo]:
        pass

    @abc.abstractmethod
    def get_account_transaction(self, account_number: str) -> list[TransactionInfo]:
        pass

    @abc.abstractmethod
    def get_interacted_users(self, telegram_id: int) -> set[str]:
        pass


class BankTransactionStorage(IBankTransactionStorage):
    def save_transaction(self, account_from: BankAccount,
                         money: int,
                         account_to: BankAccount = None,
                         card_to: Card = None) -> None:
        if account_to is None and card_to is None:
            raise Exception
        if account_to is not None:
            Transaction.objects.create(account_from=account_from, account_to=account_to, money=money)
        else:
            Transaction.objects.create(account_from=account_from, card_to=card_to, money=money)

    def get_card_transaction(self, number: str) -> list[TransactionInfo]:
        transactions = (
            Transaction.objects.filter(card_to__number=number)
                .order_by("-created_at")
                .values("money", "account_from__owner__name", "card_to__bank_account__owner__name", "created_at")
        )

        return [
            TransactionInfo(
                user_to=t["card_to__bank_account__owner__name"],
                user_from=t["account_from__owner__name"],
                money=t["money"],
                date=t["created_at"]
            )
            for t in transactions
        ]

    def get_account_transaction(self, number: str) -> list[TransactionInfo]:
        transactions = (
            Transaction.objects.filter(Q(account_to__number=number) | Q(account_from__number=number))
                .order_by("-created_at")
                .values("money",
                        "account_from__owner__name",
                        "account_to__owner__name",
                        "created_at",
                        "card_to__bank_account__owner__name")
        )

        return [
            TransactionInfo(
                user_to=(t["account_to__owner__name"]
                         if t["account_to__owner__name"] is not None
                         else t["card_to__bank_account__owner__name"]),
                user_from=t["account_from__owner__name"],
                money=t["money"],
                date=t["created_at"]
            )
            for t in transactions
        ]

    def get_interacted_users(self, user_id: int) -> set[str]:
        transactions_from = Transaction.objects.filter(Q(account_from__owner__id=user_id)).values(
            "card_to__bank_account__owner__name", "account_to__owner__name"
        )

        transactions_to_card = Transaction.objects.filter(Q(card_to__bank_account__owner__id=user_id)).values(
            "account_from__owner__name"
        )

        transactions_to_account = Transaction.objects.filter(Q(account_to__owner__id=user_id)).values(
            "account_from__owner__name"
        )

        all_users = set()

        for tr in transactions_from:
            if tr["card_to__bank_account__owner__name"]:
                all_users.add(tr["card_to__bank_account__owner__name"])
            if tr["account_to__owner__name"]:
                all_users.add(tr["account_to__owner__name"])

        for tr in transactions_to_card:
            if tr["account_from__owner__name"]:
                all_users.add(tr["account_from__owner__name"])

        for tr in transactions_to_account:
            if tr["account_from__owner__name"]:
                all_users.add(tr["account_from__owner__name"])

        return all_users
