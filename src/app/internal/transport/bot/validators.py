from functools import wraps
from telegram import Update
from telegram.ext import ContextTypes
from app.internal.models.user import User
from app.internal.transport.bot.messages import phone_not_found_error


def validate_phone_number(func):
    @wraps(func)
    async def wrapper(update: Update, context: ContextTypes.DEFAULT_TYPE):
        user = await User.objects.filter(id=update.effective_user.id).afirst()
        if not user.phone_number:
            await update.message.reply_text(phone_not_found_error())
            return

        return await func(update, context)

    return wrapper
