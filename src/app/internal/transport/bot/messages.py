from app.internal.bank_account.db.models import BankAccount
from app.internal.card.db.models import Card


def help_response() -> str:
    return ("╰(▔∀▔)╯\n"
            "Я умею:\n"
            "/help - помогать тебе\n"
            "/start - сохранить иноформацию о тебе\n"
            "/me - предоставлять информацию о тебе\n"
            "/set_phone - сохранить твой номер телефона\n"
            "/card - отдать информацию о карте\n"
            "/bank_account отдать информацию о банковском счете\n"
            "/get_favorite - показывать список твоих избранных\n"
            "/add_to_favorite - добавить пользователя в список избранных\n"
            "/send_by_name - перевести деньги по имени пользователя\n"
            "/send_by_account - перевести деньги по номеру счета\n"
            "/send_by_card - перевести деньги по номеру карты\n"
            "/interacted_users - взаимодействия\n"
            "/account_statement - выписка со счета\n"
            "/card_statement - выписка с карты\n"
            "/set_password - установить пароль\n"
            )


def start_response() -> str:
    return ("(＃￣ω￣)\n"
            "Теперь я тебя знаю!\n"
            )


def save_phone_asking() -> str:
    return ("(▽◕ ᴥ ◕▽)\n"
            "Отправь свой номер, нажав на кнопку...\n"
            )


def save_phone_response() -> str:
    return ("(´• ω •`) ♡\n"
            "Твой номер теперь у меня!\n"
            )


def me_response(id: int, name: str, phone: str) -> str:
    return ("о_О\n"
            "Вот что я знаю о тебе:\n"
            f"Id: {id}\n"
            f"Name: {name}\n"
            f"Phone: {phone}\n"
            )


def phone_not_found_error() -> str:
    return ("ฅ(^◕ᴥ◕^)ฅ\n"
            "Введи свой номер телефона /set_phone\n"
            "Без него я не могу выполнить эту команду\n"
            )


def error() -> str:
    return ("＼(º □ º l|l)/\n"
            "Что-то пошло не так\n"
            "Я скоро это исправлю\n"
            )


def user_not_found_error() -> str:
    return ("(￣ヘ￣)\n"
            "Я тебя не знаю\n"
            "Давай сначала познакомимся /start\n"
            )


def card_data(card: Card, money: float) -> str:
    return ("о_О\n"
            "Вот твоя карта\n"
            f"Код: {card.code}\n"
            f"Действует до: {card.expiry}\n"
            f"Секретный код: {card.code}\n"
            f"ДЕНЬГИ {money}\n"
            )


def card_not_found() -> str:
    return ("(￣ヘ￣)\n"
            "Нет такой карты"
            )


async def bank_account_data(bank_account: BankAccount) -> str:
    return ("о_О\n"
            "Вот твой банковский аккауни\n"
            f"Номер: {bank_account.number}\n"
            f"ДЕНЬГИ {bank_account.money}\n"
            )


def bank_account_not_found() -> str:
    return ("(￣ヘ￣)\n"
            "Нет банковского аккаунта\n"
            )


def card_number_not_exist() -> str:
    return ("ฅ(^◕ᴥ◕^)ฅ\n"
            "Введи первым аргументом номер своей карты\n"
            "Вот так: /card 12345"
            )


def name_not_exist() -> str:
    return ("ฅ(^◕ᴥ◕^)ฅ\n"
            "Введи первым аргументом имя пользователя\n"
            "Вот так: /comand Kek"
            )


def send_money_by_name_argument_error() -> str:
    return ("ฅ(^◕ᴥ◕^)ฅ\n"
            "Введи первым аргументом имя пользователя, а вторым сумму перевода\n"
            "Вот так: /send_by_name Kek 5"
            )


def send_money_by_account_argument_error() -> str:
    return ("ฅ(^◕ᴥ◕^)ฅ\n"
            "Введи первым аргументом номер банковского счета, а вторым сумму перевода\n"
            "Вот так: /send_by_name 123 5"
            )


def send_money_by_card_argument_error() -> str:
    return ("ฅ(^◕ᴥ◕^)ฅ\n"
            "Введи первым аргументом номер карты, а вторым сумму перевода\n"
            "Вот так: /send_by_name 1234 5"
            )


def favorite_user_list(user_names) -> str:
    if len(user_names) == 0:
        return ("(￣ヘ￣)\n"
                "У тебя нет пользователей в избранных\n"
                )
    return ("ฅ(^◕ᴥ◕^)ฅ\n"
            "Вот список твоих избранных\n"
            "\n".join(map(lambda x: "@" + x, user_names))
            )


def add_user_to_favorite() -> str:
    return ("(＃￣ω￣)\n"
            "Добавил пользователя в список избранных\n"
            )


def remove_user_from_favorite() -> str:
    return ("(＃￣ω￣)\n"
            "Удалил пользователя из списока избранных\n"
            )


def send_money_succes() -> str:
    return ("(＃￣ω￣)\n"
            "Перевод успешно завершен\n"
            )


def user_already_exist_in_favorite_error() -> str:
    return ("(￣ヘ￣)\n"
            "Этот пользователь уже в списке избранных\n"
            )


def user_not_exist_in_favorite_error() -> str:
    return ("(￣ヘ￣)\n"
            "Этого пользователя нет в списке твоих избранных\n"
            )


def interacted_user_list(user_names) -> str:
    if len(user_names) == 0:
        return ("(￣ヘ￣)\n"
                "Tы не переводил деньги и тебе тоже\n"
                )
    return ("ฅ(^◕ᴥ◕^)ฅ\n"
            "Вот с кем ты взаимодействовал\n"
            "\n".join(map(lambda x: "@" + x, user_names))
            )


def arg_error() -> str:
    return ("ฅ(^◕ᴥ◕^)ฅ\n"
            "Неправильно заданы аргументы"
            )


def get_statement(transactions) -> str:
    if len(transactions) == 0:
        return ("(￣ヘ￣)\n"
                "Tы не переводил деньги и тебе тоже\n"
                )
    transactions = [f"От {t.user_from} {t.user_to} {t.money} {t.date.strftime('%d/%m/%y %I:%M')}" for t in transactions]

    return "\n".join(transactions)


def set_password_message() -> str:
    return ("ฅ(^◕ᴥ◕^)ฅ\n"
            "Пароль установлен"
            )
