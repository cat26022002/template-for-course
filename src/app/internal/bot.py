from django.conf import settings
from telegram.ext import ApplicationBuilder, CommandHandler, MessageHandler, filters, CallbackContext

from app.internal.transaction.domain.services import NoAccountError, NoCardError
from app.internal.transaction.presentation.bot import account_statement, card_statement, interacted_users
from app.internal.bank_account.presentation.bot import get_bank_account, send_to_account, send_to_card, send_to_user
from app.internal.transport.bot.messages import help_response, error, bank_account_not_found, card_not_found, \
    user_not_found_error
from app.internal.user.domain.services import NoUserError
from app.internal.user.presentation.bot import start, save_phone, save_phone_request, get_favourites, add_to_favourites, \
    remove_from_favourites, me
from app.internal.authentication.presentation.bot import set_password
from telegram import Update


def run() -> None:
    application = ApplicationBuilder().token(settings.TG_BOT_TOKEN).build()
    add_handlers(application)
    application.run_polling()


def help(update: Update, context: CallbackContext) -> None:
    update.message.reply_text(text=help_response())


async def handle_error(update: Update, context: CallbackContext) -> None:
    if isinstance(context.error,NoUserError):
        await context.bot.send_message(chat_id=update.effective_chat.id, text=user_not_found_error())
        return
    if isinstance(context.error, NoAccountError):
        await context.bot.send_message(chat_id=update.effective_chat.id, text=bank_account_not_found())
        return
    if isinstance(context.error, NoCardError):
        await context.bot.send_message(chat_id=update.effective_chat.id, text=card_not_found())
        return
    print(context.error)

    await context.bot.send_message(chat_id=update.effective_chat.id, text=error())


def add_handlers(application) -> None:
    application.add_error_handler(handle_error)
    application.add_handler(MessageHandler(filters.CONTACT, save_phone))
    application.add_handler(CommandHandler("help", help))
    application.add_handler(CommandHandler("set_phone", save_phone_request))
    application.add_handler(CommandHandler("bank_account", get_bank_account))
    application.add_handler(CommandHandler("start", start))
    application.add_handler(CommandHandler("me", me))
    application.add_handler(CommandHandler("get_favorite", get_favourites))
    application.add_handler(CommandHandler("add_to_favorite", add_to_favourites))
    application.add_handler(CommandHandler("remove_from_favorite", remove_from_favourites))
    application.add_handler(CommandHandler("send_by_name", send_to_user))
    application.add_handler(CommandHandler("send_by_account", send_to_account))
    application.add_handler(CommandHandler("send_by_card", send_to_card))
    application.add_handler(CommandHandler("interacted_users", interacted_users))
    application.add_handler(CommandHandler("account_statement", account_statement))
    application.add_handler(CommandHandler("card_statement", card_statement))
    application.add_handler(CommandHandler("set_password", set_password))
