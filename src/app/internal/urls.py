from django.urls import path

from app.internal.transport.rest.handlers import UserView

urlpatterns = [
    path("me/", UserView.as_view(), name="me"),
]
