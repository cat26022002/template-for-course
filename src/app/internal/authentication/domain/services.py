import abc
import jwt
from app.internal.authentication.db.models import Token
from app.internal.authentication.domain.entities import AuthSchema
from app.internal.user.db.models import User
from config import settings
import pytz
import datetime


class IAuthRepository(abc.ABC):
    @abc.abstractmethod
    def generate_access_and_refresh_tokens(self, user: User) -> AuthSchema:
        pass

    @abc.abstractmethod
    def get_token(self, refresh_token: str) -> Token | None:
        pass

    @abc.abstractmethod
    def revoke_all_tokens(self, user: User) -> None:
        pass

    @abc.abstractmethod
    def revoke_token(self, issued_token: Token) -> None:
        pass


class AuthService:
    def __init__(self, auth_repository: IAuthRepository):
        self._auth_repository = auth_repository

    def generate_access_and_refresh_tokens(self, user: User) -> AuthSchema:
        return self._auth_repository.generate_access_and_refresh_tokens(user)

    def get_token(self, refresh_token: str) -> Token | None:
        return self._auth_repository.get_token(refresh_token)

    def revoke_all_tokens(self, user: User) -> None:
        self._auth_repository.revoke_all_tokens(user)

    def revoke_token(self, issued_token: Token) -> None:
        self._auth_repository.revoke_token(issued_token)

    def has_token_expired(self, token: str) -> bool:
        try:
            payload = jwt.decode(token, settings.JWT_SECRET_KEY, algorithms=["HS256"])
        except jwt.exceptions.ExpiredSignatureError:
            return True

        expire_date = payload.get("expire")

        if expire_date is None:
            return True

        return datetime.datetime.fromtimestamp(expire_date, tz=pytz.UTC) < pytz.UTC.localize(datetime.datetime.utcnow())

    def get_id(self, token: str) -> int | None:
        try:
            payload = jwt.decode(token, settings.JWT_SECRET_KEY, algorithms=["HS256"])
        except jwt.exceptions.ExpiredSignatureError:
            return None

        return payload.get("id")
