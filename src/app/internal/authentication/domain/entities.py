from datetime import datetime
from ninja import Schema
from pydantic.main import BaseModel

from app.internal.user.domain.entities import UserOut


class AuthUserModel(BaseModel):
    name: str
    password: str


class AuthSchema(BaseModel):
    access_token: str
    refresh_token: str


class RefreshSchema(BaseModel):
    refresh_token: str


class IssuedTokenSchema(Schema):
    jti: str
    user: UserOut
    created_at: datetime
    revoked: bool = False
