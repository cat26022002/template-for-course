from app.internal.authentication.domain.entities import AuthSchema
from app.internal.authentication.domain.services import IAuthRepository
from app.internal.user.db.models import User
from app.internal.authentication.db.models import Token
from datetime import timedelta
import pytz
import datetime
from django.conf import settings
import jwt


class AuthRepository(IAuthRepository):
    def generate_access_and_refresh_tokens(self, user: User) -> AuthSchema:
        access_token = self._generate_access_token(user.id)
        refresh_token = self._generate_refresh_token()

        Token.objects.create(jti=refresh_token, user=user)

        return AuthSchema(access_token=access_token, refresh_token=refresh_token)

    def get_token(self, refresh_token: str) -> Token | None:
        token = Token.objects.filter(jti=refresh_token)
        if not token.exists():
            return None
        return token.first()

    def revoke_all_tokens(self, user: User) -> None:
        user.refresh_tokens.update(revoked=True)
        user.save()

    def revoke_token(self, issued_token: Token) -> None:
        issued_token.revoked = True
        issued_token.save(update_fields=["revoked"])

    def _generate_access_token(self, user_id: int) -> str:
        expire = pytz.UTC.localize(datetime.datetime.utcnow()) + timedelta(settings.JWT_ACCESS_TOKEN_EXP_TIME)
        return jwt.encode({"id": user_id, "expire": int(expire.timestamp())}, settings.JWT_SECRET_KEY,
                          algorithm="HS256")

    def _generate_refresh_token(self) -> str:
        expire = pytz.UTC.localize(datetime.datetime.utcnow()) + timedelta(settings.JWT_REFRESH_TOKEN_EXP_TIME)
        return jwt.encode({"expire": int(expire.timestamp())}, settings.JWT_SECRET_KEY, algorithm="HS256")