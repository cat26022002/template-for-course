from app.internal.authentication.presentation.handlers import AuthHandlers
from ninja import NinjaAPI, Router


def get_auth_router(auth_handlers: AuthHandlers):
    router = Router(tags=["auth"])

    router.add_api_operation(
        "/login",
        ["POST"],
        auth_handlers.login,
        auth=None,
    )

    router.add_api_operation(
        "/refresh",
        ["POST"],
        auth_handlers.refresh,
        auth=None,
    )

    return router


def add_auth_router(api: NinjaAPI, auth_handlers: AuthHandlers):
    auth_router = get_auth_router(auth_handlers)
    api.add_router("/auth", auth_router)
