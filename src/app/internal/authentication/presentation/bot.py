from app.internal.transport.bot.messages import arg_error, set_password_message
from app.internal.user.domain.services import get_user_service
from telegram import Update
from telegram.ext import CallbackContext


def set_password(update: Update, context: CallbackContext) -> None:
    if len(context.args) < 1:
        update.message.reply_text(text=arg_error())
        return
    get_user_service().set_password_for_user(update.effective_user.id, context.args[0])
    update.message.reply_text(text=set_password_message())


handlers = {
    "set_password": set_password,
}
