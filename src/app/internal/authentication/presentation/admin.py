from django.contrib import admin

from app.internal.authentication.db.models import Token


@admin.register(Token)
class TokenAdmin(admin.ModelAdmin):
    pass
