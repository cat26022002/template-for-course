from http import HTTPStatus

from django.http import HttpRequest, JsonResponse
from ninja.security import HttpBearer

from app.internal.authentication.domain.entities import AuthUserModel, RefreshSchema
from app.internal.authentication.domain.services import AuthService
from app.internal.user.domain.services import UserService


class AuthHandlers:
    def __init__(self, auth_service: AuthService, user_service: UserService):
        self._auth_service = auth_service
        self._user_service = user_service

    def login(self, request: HttpRequest, user: AuthUserModel):
        db_user = self._user_service.get_user_by_username(user.name)
        if db_user is None or not self._user_service.check_password(db_user, password=user.password):
            return JsonResponse({"detail": "Unauthorized"}, status=HTTPStatus.UNAUTHORIZED)

        return self._auth_service.generate_access_and_refresh_tokens(db_user)

    def refresh(self, request: HttpRequest, token: RefreshSchema):
        issued_token = self._auth_service.get_token(token.refresh_token)
        if issued_token is None:
            return JsonResponse({"detail": "Invalid token"}, status=HTTPStatus.FORBIDDEN)

        if issued_token.revoked:
            self._auth_service.revoke_all_tokens(issued_token.user)
            return JsonResponse({"detail": "Invalid token"}, status=HTTPStatus.FORBIDDEN)

        self._auth_service.revoke_token(issued_token)
        if self._auth_service.has_token_expired(token.refresh_token):
            return JsonResponse({"detail": "Invalid token"}, status=HTTPStatus.FORBIDDEN)

        return self._auth_service.generate_access_and_refresh_tokens(issued_token.user)


class HTTJWTAuth(HttpBearer):
    def __init__(self, auth_service: AuthService, user_service: UserService):
        super().__init__()
        self._auth_service = auth_service
        self._user_service = user_service

    def authenticate(self, request: HttpRequest, token: str) -> str | None:
        if self._auth_service.has_token_expired(token):
            return None

        telegram_id = self._auth_service.get_id(token)
        if not telegram_id:
            return None

        user = self._user_service.get_user(telegram_id)
        request.user = user

        return token
